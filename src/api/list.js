import request from '@/util/request'
// 获取页面详情
export function formPageGetDetail(templateDataId) {
  return request({
    url: `/formPage/fieldsDetail/${templateDataId}`,
    method: 'get'
  })
}
// 设置取消表头
export function formPageSetListHead(data) {
  return request({
    url: `/formPage/setListHead`,
    method: 'post',
    data

  })
}

// 设置取消预览
export function formPageSetPreview(data) {
  return request({
    url: `/formPage/setPreview`,
    method: 'post',
    data

  })
}
/**
 * 提交表单数据
 * @param data
 */
// 生成器 新增提交
export function formPageDataSubmit(data) {
  return request({
    url: `/formData/dataAdd`,
    method: 'post',
    data
  })
}

// 生成器 查询数据接口
export function formDataDataQuery(data) {
  return request({
    url: `/formData/dataQuery`,
    method: 'post',
    data
  })
}
// 生成器 删除数据接口
export function formDataDelete(data) {
  return request({
    url: `formData/dataDelete`,
    method: 'post',
    data
  })
}
// 生成器 编辑数据接口
export function formDataUpdate(data) {
  return request({
    url: `/formData/dataUpdate`,
    method: 'post',
    data
  })
}
// 生成器 复制数据接口
export function formDataCopy(data) {
  return request({
    url: `/formData/dataCopy`,
    method: 'post',
    data
  })
}
// 添加操作项
export function addOperation(data) {
  return request({
    url: '/formField/addOperation',
    method: 'post',
    data
  })
}
// 更新操作项
export function updateOperation(data) {
  return request({
    url: '/formField/updateOperation',
    method: 'post',
    data
  })
}
// 删除操作项
export function deleteOperation(dataKey) {
  return request({
    url: `/formField/deleteOperation?dataKey=${dataKey}`,
    method: 'get'
  })
}
// 获取操作项列表
export function getOperations(templateDataId) {
  return request({
    url: `/formField/getOperations?templateDataId=${templateDataId}`,
    method: 'get'
  })
}
// 添加操作资源项
export function addOperationResource(data) {
  return request({
    url: `/resource/addOperationConfig`,
    method: 'post',
    data
  })
}
//  编辑操作资源项
export function editOperationConfig(data) {
  return request({
    url: `/resource/editOperationConfig`,
    method: 'post',
    data
  })
}
// 操作资源项列表
export function queryOperationConfig(data) {
  return request({
    url: `/resource/queryOperationConfig`,
    method: 'post',
    data
  })
}
//  自定义操作项触发工作流
export function customizeTrigger(data) {
  return request({
    url: `/formData/customizeTrigger?dataKey=${data.dataKey}&mongoDataId=${data.mongoDataId}`,
    method: 'get',
    data
  })
}
