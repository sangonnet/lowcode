import request from '@/util/request'
// export function login(username, password) {
//   return request({
//     url: '/user/login',
//     method: 'post',
//     data: {
//       username,
//       password
//     }
//   })
// }

/**
 * 通过token获取用户信息
 * @param token
 */
export function getInfo() {
  return request({
    url: '/api/user/info',
    method: 'get'
  })
}
/**
 * 通过token获取用户信息
 * @param token
 */
export function menu() {
  return request({
    url: '/api/user/menu',
    method: 'get'
  })
}
/**
 * 用户登出
 * @param token
 */
// export function logout() {
//   const data = {}
//   return request({
//     url: '/user/logout',
//     method: 'post',
//     data
//   })
// }

