import mapper from '@/components/expression/map'
import { formatDate } from '@/util/index'
// 当前字段的字段依赖表
const curFieldDepTable = {
  curDataKey: '',
  dependencies: []
}
// 当前的表单字段映射表

let curFormFieldMapper = []
let specialComps = []
// 构建器生成表单字段映射关系表
export function setFormFieldMapper(curCompsData, sampleCompsData) {
  const allComps = []
  curCompsData.forEach(m => {
    m.type === 'grid' && (allComps.push(...m.columns.list))
  })
  specialComps = []
  curFormFieldMapper = sampleCompsData.reduce((arr, item) => {
    allComps.forEach(n => {
      if (item.dataKey === n.dataKey && n.options && n.options.defaultValue !== undefined) {
        if (n.type === 'member' || n.type === 'relatedChoice' || n.type === 'date') {
          dealSpecialCompsData(n)
          specialComps.push({
            curItem: n,
            dataKey: n.dataKey,
            value: n.type === 'member' ? n.options.memberValue : n.type === 'date' ? n.options.dateValue : ''
          })
        }
        const value = n.type === 'member' ? n.options.memberValue : n.type === 'date' ? n.options.dateValue : n.options.defaultValue
        arr.push({
          dataKey: item.dataKey,
          value
        })
      }
    })
    return arr
  }, [])
}
// 生成器初始化计算数据
export function generateInitData(curCompsData) {
  const allComps = []
  curCompsData.forEach(m => {
    m.type === 'grid' && (allComps.push(...m.columns.list))
  })
  const expressionComps = allComps.filter(item => item.options && item.options.expressionData)
  const dealsDataKeys = []
  expressionComps.forEach(n => {
    dealNestExpression(n, expressionComps, dealsDataKeys)
  })
}
// 表达式嵌套递归处理(利用树状结构，从底部往上赋值)
export async function dealNestExpression(curItem, expressionComps, dealsDataKeys) {
  const deps = getFieldDeps(curItem.options.expressionData)
  curItem.deps = deps
  const curMapItem = curFormFieldMapper.filter(item => item.dataKey === curItem.dataKey)[0]
  if (!deps.length) {
    curMapItem.value = eval(replaceKeyWord(curItem.options.expressionData, curItem.dataKey))
    if (!dealsDataKeys.includes(curMapItem.dataKey)) {
      dealsDataKeys.push(curMapItem.dataKey)
    }
  } else {
    if (dealsDataKeys.every(item => dealsDataKeys.includes(item))) {
      curMapItem.value = eval(replaceKeyWord(curItem.options.expressionData, curItem.dataKey))
      if (!dealsDataKeys.includes(curMapItem.dataKey)) {
        dealsDataKeys.push(curMapItem.dataKey)
      }
    } else {
      deps.forEach(i => {
        const curCompData = expressionComps.find(item => item.dataKey === i)
        if (curCompData) {
          dealNestExpression(curCompData, expressionComps, dealsDataKeys)
        }
      })
    }
  }
}
// 生成器实时生成表单字段映射关系表
export function setGenerateFormFieldMapper(models, needDealedItem) {
  const result = []
  Object.entries(models).forEach(item => {
    const [key, value] = item
    if (needDealedItem && needDealedItem.dataKey === key) {
      dealSpecialCompsData(needDealedItem, value)
      result.push({
        dataKey: key,
        value: needDealedItem.type === 'member' ? needDealedItem.options.memberValue : needDealedItem.options.dateValue
      })
    } else {
      const specialItem = specialComps.find(m => m.dataKey === key)
      if (specialItem !== undefined) {
        const curItem = specialItem.curItem
        dealSpecialCompsData(curItem, value)
        specialItem.value = curItem.type === 'member' ? curItem.options.memberValue : curItem.options.dateValue
        result.push(specialItem)
      } else {
        result.push({
          dataKey: key,
          value
        })
      }
    }
  })
  curFormFieldMapper = result
}
// 特殊组件数据二次处理
function dealSpecialCompsData(curItem, value) {
  let curValue = curItem.options.defaultValue
  if (value !== undefined) {
    curValue = value
  }
  if (curItem.type === 'member') {
    if (typeof curValue === 'string') {
      if (!curValue.length) {
        curValue = []
      } else {
        curValue = curValue.split(',')
      }
    }
    // 成员
    curItem.options.memberValue = curValue.reduce((result, item, index) => {
      if (index === curValue.length - 1) {
        result += curItem.options.memberData.filter(m => m.userId === item)[0].name
      } else {
        result = result + curItem.options.memberData.filter(m => m.userId === item)[0].name + ','
      }
      return result
    }, '')
  } else if (curItem.type === 'relatedChoice') {
    console.log(curItem.options)
    // 关联选项
    // curItem.options.relatedChoiceLabel.forEach(r => {
    //   Object.keys(r).forEach(d => {
    //     curItem.options.field.forEach(f => {
    //       if (f.dataKey === d && f.type === 'member') {
    //         r[d] = r[d].reduce((result, item, index) => {
    //           if (index === curValue.length - 1) {
    //             result += curItem.options.memberData.filter(m => m.userId === item)[0].name
    //           } else {
    //             result = result + curItem.options.memberData.filter(m => m.userId === item)[0].name + ','
    //           }
    //           return result
    //         }, '')
    //       } else if (f.dataKey === d && f.type === 'date') {
    //         r[d]= formatDate(new Date(r[d]), f.options.format)
    //       }
    //     })
    //   })
    // })
  } else if (curItem.type === 'date') {
    // 日期
    if (curValue !== '') {
      curItem.options.dateValue = formatDate(new Date(curValue), curItem.options.format)
    }
  }
}
// 校验表达式
export function validateExpression(expression, formData) {
  try {
    if (!hasClosedDependence(formData)) {
      if (eval(expression) !== undefined) {
        console.log(expression, 'expression')
        console.log('执行结果:', eval(expression))
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  } catch (e) {
    console.log('报错:', e)
    return false
  }
}
// 替换表达式中的关键字
export function replaceKeyWord(expressionData, curDataKey) {
  curFieldDepTable.curDataKey = curDataKey
  let finalExpression = ''
  if (expressionData.lineData.length) {
    expressionData.lineData.forEach(item => {
      const curLineExpression = item.curLineExpression
      finalExpression += replaceOneLineKeyWord(curLineExpression, item.curLineMarks)
    })
  } else {
    finalExpression = expressionData.expression
  }
  return finalExpression
}
// 替换每行表达式的关键字
function replaceOneLineKeyWord(expressionStr, markArr) {
  let count = 0 // 记录已经替换的造成的index差值
  markArr.forEach((item, index) => {
    let curFrom = 0 // 记录当前标识的实际初始位置
    let curTo = 0 // 记录当前标识的实际结束位置
    if (index === 0) {
      curFrom = item.from
      curTo = item.to
    } else {
      curFrom = item.from + count
      curTo = item.to + count
    }
    const keyword = expressionStr.substring(curFrom, curTo)
    if (item.dataKey !== '') {
      // 替换字段关键字
      const curItem = curFormFieldMapper.filter(i => i.dataKey === item.dataKey)[0]
      const value = curItem.value
      const dataKey = curItem.dataKey
      if (!curFieldDepTable.dependencies.includes(dataKey)) {
        curFieldDepTable.dependencies.push(dataKey)
      }
      let replaceStr = ''
      if (typeof value === 'string') {
        replaceStr = `"${value}"`
      } else if (typeof value === 'number') {
        replaceStr = value + ''
      } else {
        replaceStr = JSON.stringify(value)
      }
      const arr = expressionStr.split('')
      arr.splice(curFrom, curTo - curFrom, ...replaceStr.split(''))
      expressionStr = arr.join('')
      count = count + (replaceStr.length - keyword.length)
    } else {
      // 替换函数关键字
      const fnName = mapper.filter(m => m.keyword === keyword)[0].fnName
      const arr = expressionStr.split('')
      arr.splice(curFrom, curTo - curFrom, ...fnName.split(''))
      expressionStr = arr.join('')
      count = count + item.fnType.length + 1
    }
  })
  console.log('转换成的js代码：', expressionStr)
  return expressionStr
}
// 判断不同字段之间是否有闭合依赖关系(搜集依赖)
function hasClosedDependence(formData) {
  const key = curFieldDepTable.curDataKey
  const deps = curFieldDepTable.dependencies
  const allExpressions = formData.reduce((arr, item) => {
    if (item.type === 'grid') {
      arr.push(...item.columns.list.reduce((subArr, m) => {
        if (m.options && m.options.expressionData) {
          subArr.push({
            dataKey: m.dataKey,
            expressionData: m.options.expressionData
          })
        }
        return subArr
      }, []))
    }
    return arr
  }, [])
  if (!allExpressions.length) {
    return false
  }
  for (let i = 0; i < deps.length; i++) {
    const curItem = allExpressions.filter(m => m.dataKey === deps[i])
    if (!curItem.length) {
      return false
    }
    const curExpressionData = curItem[0].expressionData
    if (getFieldDeps(curExpressionData).includes(key)) {
      return true
    }
  }
  return false
}
// 获取字段的依赖项数组
function getFieldDeps(expressionData) {
  const result = []
  if (expressionData.lineData.length) {
    expressionData.lineData.forEach(item => {
      item.curLineMarks.forEach(m => {
        if (m.dataKey !== '') {
          const curDataKey = curFormFieldMapper.filter(i => i.dataKey === m.dataKey)[0].dataKey
          if (!result.includes(curDataKey)) {
            result.push(curDataKey)
          }
        }
      })
    })
  }
  return result
}
// 设置当前字段的订阅者
export function setCurFieldSubs(curCompsData, curDataKey) {
  const allComps = []
  const subs = []
  curCompsData.forEach(m => {
    m.type === 'grid' && (allComps.push(...m.columns.list))
  })
  allComps.forEach(item => {
    if (item.options && item.options.expressionData) {
      if (getFieldDeps(item.options.expressionData).includes(curDataKey)) {
        subs.push(item.dataKey)
      }
    }
  })
  return subs
}

